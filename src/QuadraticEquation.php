<?php

namespace Last1971\QuadraticEquation;

use Exception;

class QuadraticEquation
{
    private float $epsilon = 0.00001;

    /**
     * @return float[]
     * @throws Exception
     */
    public function solve(float $a, float $b, float $c): array
    {
        if (is_nan($a) || is_nan($b) || is_nan($c)) {
            throw new Exception('Some argument is NaN');
        }
        if (abs($a) < $this->epsilon) {
            throw new Exception('Argument A cannot be zero');
        }
        $res = [];
        $discriminant = $b * $b - 4 * $a * $c;
        if ($discriminant > $this->epsilon) {
            $res[] = (-$b + sqrt($discriminant)) / 2 * $a;
            $res[] = (-$b - sqrt($discriminant)) / 2 * $a;
        } elseif (abs($discriminant) < $this->epsilon) {
            $res[] = (-$b) / 2 * $a;
        }
        return $res;
    }
}