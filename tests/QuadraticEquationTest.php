<?php

namespace Tests;

use Last1971\QuadraticEquation\QuadraticEquation;
use PHPUnit\Framework\TestCase;

class QuadraticEquationTest extends TestCase
{
    private QuadraticEquation $quadraticEquation;

    public function setUp(): void
    {
        parent::setUp();
        $this->quadraticEquation = new QuadraticEquation();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        unset($this->quadraticEquation);
    }

    public function testSolveHasNoRoots(): void
    {
        $this->assertEquals([], $this->quadraticEquation->solve(1, 0, 1));
    }

    public function testSolveHasTwoRoots(): void
    {
        $this->assertEquals([1, -1], $this->quadraticEquation->solve(1, 0, -1));
    }

    public function testSolveHasOneRoot(): void
    {
        $this->assertEquals([-1], $this->quadraticEquation->solve(1,2, 0.999999));
    }

    public function testSolveAIsNotEqualZero(): void
    {
        $this->expectException(\Exception::class);
        $this->quadraticEquation->solve(0, 2, 1);
    }

    /**
     * @dataProvider providerNanData
     */
    public function testSolveNanCoefficients($a, $b, $c): void
    {
        $this->expectException(\Exception::class);
        $this->quadraticEquation->solve($a, $b, $c);
    }

    /**
     * @return float[][]
     */
    public function providerNanData(): array
    {
        $nan = acos(8);
        $base = [$nan, 1];
        $res = [];
        foreach ($base as $a) {
            foreach ($base as $b) {
                foreach ($base as $c) {
                    if (is_nan($a) || is_nan($b) || is_nan($c)) $res[] = [$a, $b, $c];
                }
            }
        }
        return $res;
    }

}